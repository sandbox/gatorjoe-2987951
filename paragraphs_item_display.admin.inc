<?php
/**
 * @file
 * Admin form for Paragraphs Item Display module
 */


/**
 * @param $form
 * @param $form_state
 * @return mixed
 */

function paragraphs_item_display_admin_form($form, &$form_state) {
  $entities = entity_get_info('paragraphs_item');
  $form['info'] = array(
    '#type' => 'item',
    '#description' => t('This form provides a list of available paragraph bundles. Supplemental text may be added which will be displayed after the paragraph item type on the admin page. If tokens are enabled, available paragraph tokens are listed below.'),
  );

  if (!empty($entities['bundles'])) {
    foreach ($entities['bundles'] as $key => $item) {
      $form['admin'][$key] = array(
        '#type' => 'fieldset',
        '#title' => check_plain(t($key)),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );

      $form['admin'][$key]['paragraphs_item_display_' . $key] = array(
        '#type' => 'textfield',
        '#title' => t('Supplemental text'),
        '#size' => 128,
        '#maxlength' => 256,
        '#default_value' => variable_get('paragraphs_item_display_' . $key),
      );
    }

    if (module_exists('token')) {
      $form['tokens'] = array(
        '#type' => 'fieldset',
        '#title' => t('Replacement values'),
        '#description' => t('Paragraphs tokens available for substitution.'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $form['tokens']['token_tree'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('paragraphs_item'),
        '#global_types' => FALSE,
        '#click_insert' => FALSE,
      );
    }
    else {
      $form['token_tree'] = array(
        '#markup' => '<p>' . t('Enable the <a href="@drupal-token">Token module</a> to view the available token browser.', array(
            '@drupal-token' => 'http://drupal.org/project/token',
          )) . '</p>',
      );
    }

    return system_settings_form($form);
  }
  else {
    $form['empty'] = array(
      '#type' => 'item',
      '#markup' => '<b>No bundles found. Please create a paragraph bundle.</b>',
    );

    return $form;
  }


}

